/*
 * Copyright (c) 2022,2023 Dawn <callmedawn@disroot.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <getopt.h>

#include <err.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>

#include "xbi.h"

struct universe
{
	struct backend_info bd;

	XtAppContext ctx;
	Widget statchars;
	Widget pct;
	Widget timeleft;
	Widget timedone;
	XtIntervalId timer;
	long polltime;
};

/*
 * The handling of the WM protocol for closing the window is based on a
 * worked example I found; I haven't been able to find any documentation
 * that describes how it's supposed to work.
 * The translation management chapter of the Xt documentation (chapter 10
 * as of X11R6.8) covers a lot of it but is notably missing the way the
 * WM protocol described by the ICCCM is mapped into something we can
 * specify in the translation table.
 */
static void quit_cb(Widget w, XEvent *e, String *params, Cardinal *num_params)
{
	(void)e;
	(void)params;
	(void)num_params;
	XtAppSetExitFlag(XtWidgetToApplicationContext(w));
}
static XtActionsRec quit_action = { "Quit", quit_cb };
static const char *quit_translation = "<Message>WM_PROTOCOLS:Quit()\n";

static void update(struct universe *u)
{
	char buf[8];
	struct battery_info inf;
	if (xbi_update(u->bd.backend_env, &inf) != 0)
		return;
	
	if (inf.power_state == PWR_POWERED) buf[0] = 'P';
	else if (inf.power_state == PWR_UNPOWERED) buf[0] = ' ';
	else buf[0] = '?';
	if (inf.battery_state == BATT_OK) buf[1] = ' ';
	else if (inf.battery_state == BATT_CHARGING) buf[1] = 'C';
	else if (inf.battery_state == BATT_LOW) buf[1] = 'L';
	else if (inf.battery_state == BATT_CRITICAL) buf[1] = '!';
	else buf[1] = '?';
	buf[2] = '\0';
	XtVaSetValues(u->statchars, XtNlabel, buf, NULL);

	if (inf.level >= 100)
		XtVaSetValues(u->pct, XtNlabel, "100%", XtNjustify, XtJustifyRight, NULL);
	else if (inf.level >= 0)
	{
		snprintf(buf, sizeof buf, "%d%%", inf.level);
		XtVaSetValues(u->pct, XtNlabel, buf, XtNjustify, XtJustifyRight, NULL);
	}
	else
	{
		/* percentage not available, fall back to other information */
		if (inf.battery_state == BATT_OK) strlcpy(buf, "HI", sizeof buf);
		else if (inf.battery_state == BATT_LOW) strlcpy(buf, "LO", sizeof buf);
		else if (inf.battery_state == BATT_CRITICAL) strlcpy(buf, "CR", sizeof buf);
		else strlcpy(buf, "??", sizeof buf);
		XtVaSetValues(u->pct, XtNlabel, buf, XtNjustify, XtJustifyCenter, NULL);
	}

	if (inf.time_left < 0) strlcpy(buf, "--:--", sizeof buf);
	else if (inf.time_left == 0) strlcpy(buf, "", sizeof buf);
	else if (inf.time_left >= 100*60) strlcpy(buf, "99:99", sizeof buf);
	else snprintf(buf, sizeof buf, "%d:%02d",
	    inf.time_left / 60, inf.time_left % 60);
	XtVaSetValues(u->timeleft, XtNlabel, buf, NULL);

	if (u->timedone)
	{
		buf[0] = '\0';
		if (inf.time_left > 0)
		{
			time_t now = time(0);
			struct tm tm = *localtime(&now);
			tm.tm_min += inf.time_left;
			mktime(&tm);	/* normalize */
			sprintf(buf, "(%02d:%02d)", tm.tm_hour, tm.tm_min);
		}
		XtVaSetValues(u->timedone, XtNlabel, buf, NULL);
	}
}

static void timer_update(XtPointer client_data, XtIntervalId *id)
{
	(void)id;
	struct universe *u = client_data;
	update(u);
	/* timeouts are always one-shot, so re-arm */
	u->timer = XtAppAddTimeOut(u->ctx, u->polltime, timer_update, u);
}

static void select_update(XtPointer client_data, int *src, XtInputId *id)
{
	struct universe *u = client_data;
	(void)src;
	(void)id;
	update(u);
}

/*
 * The argument list is pairs of (int, string pointer) representing 
 * characters and maximum counts that may appear in the label, terminated
 * by a negative value.
 * The widget is set to the smallest size sufficient to contain any string
 * containing at most the given number of characters taken from each of
 * the strings in the argument list.
 */
void set_size(Widget w, ...)
{
	XFontStruct *fs;
	Dimension ih, iw;
	int n;
	Arg args[4];
	va_list va;
	int totalwidth = 0;
	int count;

	va_start(va, w);

	n = 0;
	XtSetArg(args[n], XtNfont, &fs); n++;
	XtSetArg(args[n], XtNinternalHeight, &ih); n++;
	XtSetArg(args[n], XtNinternalWidth, &iw); n++;
	XtGetValues(w, args, n);

	while ((count = va_arg(va, int)) > 0)
	{
		char *chars = va_arg(va, char *);
		int maxwidth = 0;

		while (*chars)
		{
			char lbuf[2] = { *chars, 0 };
			int cw = XTextWidth(fs, lbuf, 1);
			if (cw > maxwidth)
				maxwidth = cw;
			chars++;
		}
		totalwidth += count * maxwidth;
	}

	n = 0;
	XtSetArg(args[n], XtNwidth, totalwidth + 2*iw); n++;
	XtSetArg(args[n], XtNheight, fs->max_bounds.ascent + fs->max_bounds.descent + 2*ih); n++;
	XtSetValues(w, args, n);
}

void usage_and_die(const char *myname)
{
	printf("Usage: %s [-e] [-p polltime] [Xt toolkit option...]\n", myname);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	struct universe universe = {0};

	Widget status;
	Widget toplevel;

	/*
	 * Configuration accessible via either command-line options
	 * or X resources
	 */
	struct resourceopts
	{
		Boolean eflag;	/* show completion time */
		int pval;	/* poll interval */
	} ro;
	XtResource resources[] = {
		{ "pollInterval", XtCValue, XtRInt, sizeof (int), offsetof(struct resourceopts, pval), XtRImmediate, (XtPointer)30 },
		{ "endTime", XtCValue, XtRBoolean, sizeof (Boolean), offsetof(struct resourceopts, eflag), XtRImmediate, (XtPointer)FALSE },
	};
	XrmOptionDescRec options[] = {
		{ "-p", "*pollInterval", XrmoptionSepArg, NULL },
		{ "-e", "*endTime", XrmoptionNoArg, "true" },
	};

	toplevel = XtVaAppInitialize(&universe.ctx, "XBi", options, XtNumber(options), &argc, argv, NULL, NULL);
	status = XtVaCreateManagedWidget("status", formWidgetClass, toplevel,
		XtNorientation, XtorientHorizontal,
		XtNborderWidth, 0,
		XtNdefaultDistance, 2,
		NULL);
	
	XtGetApplicationResources(toplevel, &ro, resources, XtNumber(resources), NULL, 0);

	universe.bd.select_fd = -1;
	universe.bd.poll_time = ro.pval;
	universe.bd.backend_env = NULL;
	if (xbi_setup(&universe.bd) != 0)
		return 1;

	universe.statchars = XtVaCreateManagedWidget("statchars", labelWidgetClass, status,
		XtNlabel, "",
		XtNresize, FALSE,
		NULL);
	/* "<ac-status><batt-status>" */
	set_size(universe.statchars, 1, " P?", 1, " CL!?", -1);
	universe.pct = XtVaCreateManagedWidget("level", labelWidgetClass, status,
		XtNfromHoriz, universe.statchars,
		XtNlabel, "",
		XtNjustify, XtJustifyRight,
		XtNresize, FALSE,
		NULL);
	/* "nnn%" */
	set_size(universe.pct, 1, "%", 1, "1", 2, "0123456789", -1);
	universe.timeleft = XtVaCreateManagedWidget("timeleft", labelWidgetClass, status,
		XtNfromHoriz, universe.pct,
		XtNlabel, "--:--",
		XtNjustify, XtJustifyRight,
		XtNresize, FALSE,
		NULL);
	/* "hh:mm" */
	set_size(universe.timeleft, 1, ":", 4, "0123456789-", -1);
	if (ro.eflag)
	{
		universe.timedone = XtVaCreateManagedWidget("timedone", labelWidgetClass, status,
			XtNfromHoriz, universe.timeleft,
			XtNlabel, "",
			XtNjustify, XtJustifyRight,
			XtNresize, FALSE,
			NULL);
		/* "(hh:mm)" */
		set_size(universe.timedone, 1, ":", 1, "(", 1, ")", 4, "0123456789", -1);
	}

	if (universe.bd.select_fd >= 0)
	{
		XtAppAddInput(universe.ctx, universe.bd.select_fd, (XtPointer)XtInputReadMask, select_update, &universe);
	}
	else
	{
		int poll_sec = universe.bd.poll_time;
		if (poll_sec < 5) poll_sec = 5;
		universe.polltime = poll_sec * 1000l;
		universe.timer = XtAppAddTimeOut(universe.ctx, universe.polltime, timer_update, &universe);
	}
	XtRealizeWidget(toplevel);

	/*
	 * As noted above, WM protocol handling is based on how a random
	 * bit of code I found did it, not on documentation about how it's
	 * supposed to work.
	 * Note that XSetWmProtocols needs a window to actually exist, i.e.
	 * it must be done after the widget is realized.
	 */
	Atom wmproto = XInternAtom(XtDisplay(toplevel), "WM_DELETE_WINDOW", FALSE);
	XtAppAddActions(universe.ctx, &quit_action, 1);
	XtOverrideTranslations(toplevel, XtParseTranslationTable(quit_translation));
	XSetWMProtocols(XtDisplay(toplevel), XtWindow(toplevel), &wmproto, 1);

	XtAppMainLoop(universe.ctx);
}

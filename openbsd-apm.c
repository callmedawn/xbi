/*
 * Copyright (c) 2022 Dawn <callmedawn@disroot.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>

#include <err.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/event.h>
#include <sys/time.h>

#include <machine/apmvar.h>

#include "xbi.h"

struct apm_data
{
	int apm_fd;
	int kq;
};

/* Returns zero on success */
int xbi_setup(struct backend_info *data)
{
	struct apm_data *env;

	struct kevent ke;

	env = malloc(sizeof *env);
	if (env == NULL)
	{
		warn("malloc");
		goto fail_alloc;
	}

	env->apm_fd = open("/dev/apm", O_RDONLY);
	if (env->apm_fd == -1)
	{
		warn("open: /dev/apm");
		goto fail_open_apm;
	}

	env->kq = kqueue();
	if (env->kq == -1)
	{
		warn("kqueue");
		goto fail_kqueue;
	}

	EV_SET(&ke, env->apm_fd, EVFILT_READ, EV_ADD|EV_ENABLE|EV_CLEAR,
		0, 0, NULL);
	if (kevent(env->kq, &ke, 1, NULL, 0, NULL) == -1)
	{
		warn("kevent (setup)");
		goto fail_kevent;
	}

	data->select_fd = env->kq;
	data->poll_time = 0;
	data->backend_env = env;

	return 0;

	/* no additional cleanup if kevent() fails */
fail_kevent:
	close(env->kq);
fail_kqueue:
	close(env->apm_fd);
fail_open_apm:
	free(env);
fail_alloc:
	return -1;
}

int xbi_update(void *venv, struct battery_info *inf)
{
	struct apm_data *env = venv;

	struct apm_power_info pi;
	int rv;

	/*
	 * Read any events out from the kqueue.
	 * We do not actually need any information this collects but it's
	 * how we make it stop select()ing as readable.
	 * (If there's nothing there, that's a spurious poll, which we need
	 * to be prepared to handle anyways, so just do nothing.)
	 */
	do
	{
		struct kevent ke;
		struct timespec tmout = {0};
		rv = kevent(env->kq, NULL, 0, &ke, 1, &tmout);
		if (rv == -1)
		{
			warn("kevent (wait)");
			return -1;
		}
	}
	while (rv > 0);

	if (ioctl(env->apm_fd, APM_IOC_GETPOWER, &pi) == -1)
	{
		warn("ioctl: /dev/apm: APM_IOC_GETPOWER");
		return -1;
	}

	switch(pi.battery_state)
	{
		case APM_BATT_HIGH: inf->battery_state = BATT_OK; break;
		case APM_BATT_LOW: inf->battery_state = BATT_LOW; break;
		case APM_BATT_CRITICAL: inf->battery_state = BATT_CRITICAL; break;
		case APM_BATT_CHARGING: inf->battery_state = BATT_CHARGING; break;
		default: inf->battery_state = BATT_UNKNOWN; break;
	}

	switch(pi.ac_state)
	{
		case APM_AC_OFF: inf->power_state = PWR_UNPOWERED; break;
		case APM_AC_ON: inf->power_state = PWR_POWERED; break;
		default: inf->power_state = PWR_UNKNOWN; break;
	}

	inf->level = pi.battery_life;
	if (pi.minutes_left == (unsigned)-1)
		inf->time_left = -1;
	else
		inf->time_left = pi.minutes_left;

	return 0;
}

void xbi_cleanup(void *venv)
{
	struct apm_data *env = venv;
	close(env->kq);
	close(env->apm_fd);
	free(env);
}

/*
 * Copyright (c) 2022 Dawn <callmedawn@disroot.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <getopt.h>
#include <poll.h>

#include <err.h>

#include "xbi.h"

static int done = 0;

static int tflag, eflag;

void sighandler(int s)
{
	(void)s;
	done = 1;
}

void usage_and_die(const char *myname)
{
	printf("Usage: %s [-eft] [-p poll-interval]\n", myname);
	exit(0);
}

void print_info(void *env)
{
	struct battery_info inf;
	time_t now;
	struct tm tm;

	if (xbi_update(env, &inf) != 0)
		errx(1, "battery status failed");

	/* just in case */
	now = time(0);
	tm = *localtime(&now);

	/* wall clock time (-t) */
	if (tflag)
		printf("%02d:%02d:%02d  ", tm.tm_hour, tm.tm_min, tm.tm_sec);

	/* power state flags */
	switch(inf.power_state)
	{
	case PWR_POWERED: printf("P"); break;
	case PWR_UNPOWERED: printf(" "); break;
	default: printf("?"); break;
	}
	switch(inf.battery_state)
	{
	case BATT_OK: printf(" "); break;
	case BATT_CHARGING: printf("C"); break;
	case BATT_LOW: printf("L"); break;
	case BATT_CRITICAL: printf("!"); break;
	default: printf("?"); break;
	}

	/* level (percentage of full) */
	printf(" ");
	if (inf.level >= 100) printf("100%%");
	else if (inf.level >= 0) printf("%3d%%", inf.level);
	else if (inf.battery_state == BATT_LOW) printf(" LO ");
	else if (inf.battery_state == BATT_CRITICAL) printf(" CR ");
	else if (inf.battery_state == BATT_UNKNOWN) printf(" ?? ");
	else printf(" HI ");

	/*
	 * Everything that depends on having time left goes last, so we
	 * don't have to worry about field shifting if we skip it
	 */

	/* time left */
	printf(" ");
	if (inf.time_left < 0) printf("--:--");
	else if (inf.time_left == 0) { /* skip output */ }
	else if (inf.time_left >= 100 * 60) printf("99:99");
	else printf("%2d:%02d", inf.time_left / 60, inf.time_left % 60);

	/* time when done (-e) */
	if (eflag && inf.time_left > 0)
	{
		tm.tm_min += inf.time_left;
		mktime(&tm);    /* normalize */
		printf("  (%02d:%02d)", tm.tm_hour, tm.tm_min);
	}

	printf("\n");
}

int main(int argc, char **argv)
{
	int fflag;
	int o;

	struct backend_info bd;

	int default_poll = 0;

	fflag = 0;
	while ((o = getopt(argc, argv, "efp:t")) != -1)
	{
		switch(o)
		{
		case 'e':
			eflag = 1;
			break;
		case 'f':
			fflag = 1;
			break;
		case 'p':
			{
			long r;
			char *endptr;
			r = strtol(optarg, &endptr, 10);
			if (*endptr != '\0' || r < 0 || r > INT_MAX)
				usage_and_die(argv[0]);
			default_poll = r;
			}
			break;
		case 't':
			tflag = 1;
			break;
		default:
			usage_and_die(argv[0]);
			/* not reached */
		}
	}
	if (optind != argc)
		usage_and_die(argv[0]);

	bd.select_fd = -1;
	bd.poll_time = 30;
	bd.backend_env = NULL;
	if (xbi_setup(&bd) != 0)
		return 1;

	if (default_poll > 0)
		bd.poll_time = default_poll;

	print_info(bd.backend_env);
	if (!fflag)
		return 0;

	/* Keep watching for and reporting updates (-f) */

	signal(SIGINT, sighandler);
	signal(SIGTERM, sighandler);

	while (!done)
	{
		struct pollfd pfd;
		int nfd = 0;
		int rv;

		int tmout = bd.poll_time * 1000;
		if (tmout < 1000)
			tmout = 1000;
		
		if (bd.select_fd >= 0)
		{
			pfd.fd = bd.select_fd;
			pfd.events = POLLIN;
			tmout = -1;
			nfd = 1;
		}

		rv = poll(&pfd, nfd, tmout);
		if (rv == -1 && errno != EINTR)
			err(1, "poll");

		print_info(bd.backend_env);
	}

	return 0;
}

#
# In the professional opinion of the author, this makefile does not
# contain anything sufficiently original or non-obvious to be worth
# asserting copyright claims on.
# (N.B. The author is a software engineer, not an IP lawyer.)
# To avoid any uncertainty, this file is dedicated to the public
# domain by the author.  In jurisdictions that do not respect authors'
# right to dedicate their work to the public domain, any copyrightable
# material in this file may be used under the terms of Creative Commons
# Zero (any version at your option).
#

CFLAGS_X11 != pkg-config --cflags x11 xt xaw7
LDFLAGS_X11 != pkg-config --libs x11 xt xaw7
CFLAGS=-Wall -Wextra -std=c99 -pedantic -O2 -g $(CFLAGS_X11)
LDFLAGS=

PROGRAMS = bi xbi
MANPAGES = bi.1 xbi.1

.PHONY: all clean
all: $(PROGRAMS)

bi: bi.o openbsd-apm.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $>
xbi: xbi.o openbsd-apm.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(LDFLAGS_X11) -o $@ $> $^

clean:
	rm -f $(PROGRAMS) *.o tags

BINMODE ?= 0755
MANMODE ?= 0644
BINOWN ?= root
MANOWN ?= root
BINGRP ?= bin
MANGRP ?= bin

INSTALL ?= install

PREFIX ?= /usr/local
MANDIR ?= $(PREFIX)/man
BINDIR ?= $(PREFIX)/bin

install: $(MANPAGES) $(PROGRAMS)
	$(INSTALL) -m $(MANMODE) -o $(MANOWN) -g $(MANGRP) $(MANPAGES) $(DESTDIR)$(MANDIR)/man1/
	$(INSTALL) -m $(BINMODE) -o $(BINOWN) -g $(BINGRP) $(PROGRAMS) $(DESTDIR)$(BINDIR)/

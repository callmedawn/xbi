/*
 * Copyright (c) 2022 Dawn <callmedawn@disroot.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef H_XBI
#define H_XBI

/* frontend-backend interface */

enum battery_state
{
	BATT_OK,
	BATT_CHARGING,
	BATT_LOW,
	BATT_CRITICAL,
	BATT_UNKNOWN,
};

enum power_state
{
	PWR_POWERED,
	PWR_UNPOWERED,
	PWR_UNKNOWN,
};

struct battery_info
{
	enum battery_state battery_state;
	enum power_state power_state;

	/* percentage; negative means not available */
	int level;

	/*
         * minutes; zero means unavailable or uninteresting (don't report),
	 * negative means temporarily unavailable (report '-:--').
	 * 
	 */
	int time_left;
};

struct backend_info
{
	/*
	 * if nonnegative, a fd to select(2) or poll(2) on to see when
	 * an update is available.
	 */
	int select_fd;

	/*
	 * if nonzero, the preferred interval for polling updates, in seconds.
         * This is advisory and may be ignored or overridden by
         * other configuration.
	 * If select_fd is populated, this will normally be ignored.
	 */
	int poll_time;

	/* User data; not used or interpreted by frontend. */
	void *backend_env;
};

/* Returns zero on success */
int xbi_setup(struct backend_info *data);

/* Returns zero on success */
int xbi_update(void *env, struct battery_info *inf);

void xbi_cleanup(void *env);

#endif	/* H_XBI #include guard */
